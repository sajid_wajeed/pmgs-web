import React from "react";
import Header from "./Header";
import Footer from "./Footer";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import Registration from "../Registration/Registration";
import PatientList from "../../components/PatientRecordList/PatientList";
import HistoryFinding from "../../components/HistoryFindings/HistoryFinding";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Home from "../Home/Home";
import SideNav from "../Layout/SideNav";

const appRoutes = [
  {
    path: "/register",
    component: Registration
  },
  {
    path: "/getRecord",
    component: PatientList
  },
  {
    path: "/home/:id",
    component: Home
  },
  {
    path: "/:id/history-finding",
    component: HistoryFinding
  },
  { redirect: true, path: "/", to: "/" }
];
class Layout extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="wrapper">
        <Header />
        <div className="Main">
          <SideNav />
          <div className="main-nav">
            <Switch>
              {appRoutes.map((prop, key) => {
                return (
                  <Route
                    path={prop.path}
                    component={prop.component}
                    key={key}
                    exact={true}
                  />
                );
              })}
            </Switch>
          </div>
          <div className="more-option">
            <section className="option-list">
            <FontAwesomeIcon
              icon={["fas", "print"]}
              onClick={this.handleShow}
            />
             <FontAwesomeIcon
              icon={["fas", "trash"]}
              onClick={this.handleShow}
            />
             <FontAwesomeIcon
              icon={["fas", "edit"]}
              onClick={this.handleShow}
            />
            </section>
            <FontAwesomeIcon className="crossRotate"
              icon={["fas", "plus-circle"]}
              onClick={this.handleShow}
            />
          </div>
        </div>
        <Footer className="footer" />
      </div>
    );
  }
}

export default Layout;
